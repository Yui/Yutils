# Yutils

[![JSR](https://jsr.io/badges/@yui/utils)](https://jsr.io/@yui/utils)

- Some smaller modules

## List

- `delayCondition` - a function that waits till a user provided condition is true

- `sleep` - equivalent to linux sleep command

- `tokenBucket` - class for rate limiting purposes

- `queue` - queue
