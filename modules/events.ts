// deno-lint-ignore no-explicit-any
function joinFunctions(fns: ((...args: any[]) => any)[]): (...args: any[]) => any {
  return eval(`((functions) => {
      ${fns.map((_, i) => `const fn${i} = functions[${i}];`).join(" ")}
      return ((...args) => {
          ${fns.map((_, i) => `fn${i}(...args);`).join(" ")}
      });
  })`)(fns);
}

export type eventHandle<T extends unknown | null = unknown> = T extends null
  ? () => unknown | Promise<unknown>
  : (event: T) => unknown | Promise<unknown>;

/**
 * Creates an instance.
 *
 * @example
 * Type the events like this:
 * ```ts
 * type Events = {
 *   name: string;
 *   age?: number;
 *   nicknames: string[];
 *   school: (location: string, type: string) => void; // multiple parameters are defined like this
 *   test: null; // Null values produce empty function () => void | Promise<void>
 * };
 * event<Events>();
 * ```
 */

export type eventReturn<T extends Record<symbol | string, unknown>> = {
  on<K extends keyof T>(
    type: K,
    // deno-lint-ignore no-explicit-any
    handle: T[K] extends (...args: any) => any ? Parameters<eventHandle<T[K]>>[0] : eventHandle<T[K]>,
    once?: boolean
  ): void;
  off(type: "*" | keyof T, handle?: eventHandle): void;
  once<K extends keyof T>(
    type: K,
    // deno-lint-ignore no-explicit-any
    handle: T[K] extends (...args: any) => any ? Parameters<eventHandle<T[K]>>[0] : eventHandle<T[K]>
  ): void;
  emit<E extends keyof T>(
    type: E,
    // deno-lint-ignore no-explicit-any
    ...data: T[E] extends (...args: any) => any ? Parameters<T[E]> : T[E] extends null ? never : [T[E]]
  ): void;
};

export default function event<T extends Record<symbol | string, unknown>>(): eventReturn<T> {
  const handlersMap = new Map<keyof T, eventHandle[]>();
  const onceHandlersMap = new Map<keyof T, eventHandle[]>();
  const emittersMap = new Map<keyof T, eventHandle>();
  const onceEmittersMap = new Map<keyof T, eventHandle>();

  function recalculate(once = false) {
    for (const k in handlersMap.keys()) {
      recalculateType(k, once);
    }
  }

  function recalculateType<K extends keyof T>(type: K, once = false) {
    const handlersStore = once ? onceHandlersMap : handlersMap;
    const emittersStore = once ? onceEmittersMap : emittersMap;
    const handlers = handlersStore.get(type);

    //@ts-ignore this is fine
    if (handlers) emittersStore.set(type, joinFunctions(handlers));
  }

  const tmp: eventReturn<T> = {
    on(type, handle, once = false) {
      const store = once ? onceHandlersMap : handlersMap;
      store.get(type)?.push(handle) ?? store.set(type, [handle]);
      recalculateType(type, once);
    },
    once(type, handle) {
      return this.on(type, handle, true);
    },
    off(type, handle) {
      if (type === "*") {
        if (!handle) {
          handlersMap.clear();
          onceHandlersMap.clear();
        } else {
          handlersMap.forEach((handlers) => handlers.splice(handlers.indexOf(handle) >>> 0, 1));
          onceHandlersMap.forEach((handlers) => handlers.splice(handlers.indexOf(handle) >>> 0, 1));
        }
        recalculate();
      } else {
        if (!handle) {
          handlersMap.delete(type);
          onceHandlersMap.delete(type);
        } else {
          const handlers = handlersMap?.get(type);
          handlers?.splice(handlers.indexOf(handle) >>> 0, 1);

          const onceHandlers = onceHandlersMap?.get(type);
          onceHandlers?.splice(onceHandlers.indexOf(handle) >>> 0, 1);
        }
        recalculateType(type);
      }
    },
    emit(type, ...data) {
      //@ts-ignore x
      emittersMap.get(type)?.(...data);
      //@ts-ignore x
      onceEmittersMap.get(type)?.(...data);
      onceHandlersMap.delete(type);
      onceEmittersMap.delete(type);
    },
  };

  // Bind this to function which need to use this
  tmp.once = tmp.once.bind(tmp);
  tmp.emit = tmp.emit.bind(tmp);

  return tmp;
}
