export class QueueItem<T> {
  next?: QueueItem<T>;
  value: T;
  constructor(value: T) {
    this.value = value;
  }
}

export class Queue<T> implements Iterable<T> {
  #size = 0;
  capacity: number | undefined;
  #start: QueueItem<T> | undefined;
  #end: QueueItem<T> | undefined;

  get size(): number {
    return this.#size;
  }

  /** Default maximum size is infinite. */
  constructor(maxSize?: number) {
    this.capacity = maxSize;
  }

  *[Symbol.iterator](): IterableIterator<T> {
    let current = this.#start;

    while (current) {
      yield current.value;
      current = current.next;
    }
  }

  /** Adds an item to the queue. Items going over capacity will be thrown away. */
  add(values: T | T[]): void {
    if (this.capacity && this.#size == this.capacity) return;
    if (!Array.isArray(values)) values = [values];
    const freeSpace = this.capacity && this.capacity - this.#size;
    if (freeSpace && values.length > freeSpace) values.slice(0, freeSpace - 1);

    for (const value of values) {
      const item = new QueueItem<T>(value);

      if (this.#start) {
        if (this.#end) this.#end.next = item;
        this.#end = item;
      } else {
        this.#start = item;
        this.#end = item;
      }
      this.#size++;
    }
  }

  /** Remove next item from the queue and gives it to you if possible. */
  take<E extends number | undefined = undefined>(amount?: E): E extends undefined ? T | undefined : T[] {
    if (!amount) {
      const item = this.#start;
      //@ts-ignore this is ok
      if (!item) return;

      this.#start = this.#start?.next;
      this.#size--;

      //@ts-ignore this is ok
      return item.value;
    }

    const values: T[] = [];
    for (let i = 0; i < amount; i++) {
      const item = this.#start;
      //@ts-ignore this is ok
      if (!item) return values;

      this.#start = this.#start?.next;
      this.#size--;
      values.push(item.value);
      if (this.#size == 0) break;
    }
    //@ts-ignore this is ok
    return values;
  }

  /** Gets you next item from the queue without removing it. */
  peek(): T | undefined {
    return this.#start?.value;
  }

  /** Clears the queue */
  clear(): void {
    this.#size = 0;
    this.#start = undefined;
    this.#end = undefined;
  }

  toArray(): T[] {
    if (!this.#start) return [];
    const arr: T[] = [];

    let curr = this.#start;
    for (let i = 0; i < this.#size; i++) {
      arr[i] = curr.value;
      if (curr.next) curr = curr.next;
    }

    return arr;
  }

  /** Reverses the queue */
  reverse(): void {
    let curr = this.#start;
    let prev: typeof curr;

    while (curr) {
      const next = curr.next;
      curr.next = prev;
      prev = curr;
      curr = next;
    }
    this.#end = this.#start;
    this.#start = prev;
  }
}
