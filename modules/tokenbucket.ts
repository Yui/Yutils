import { delayCondition } from "./delayCondition.ts";

export enum TokenBucketErrorEnum {
  Capacity,
  Tokens,
}

export class TokenBucketError extends Error {
  constructor(type: keyof typeof TokenBucketErrorEnum, message: string) {
    super(message);
    this.type = type;
  }
  type: keyof typeof TokenBucketErrorEnum;
}

export class TokenBucket {
  capacity: number;
  tokens: number;
  lastFilled: number;
  fillInterval: number;
  amountPerInterval: number;

  /**
   *
   * @param capacity - max burst
   * @param fillInterval -
   * @param amountPerInterval
   */
  constructor(capacity: number, fillInterval: number, amountPerInterval: number = 1) {
    this.capacity = capacity;
    this.lastFilled = Date.now();
    this.tokens = capacity;
    this.fillInterval = fillInterval;
    this.amountPerInterval = amountPerInterval;
  }

  /** This will not allow you to go over the capacity. */
  addToken(amount = 1) {
    this.#refill();

    if (this.tokens < this.capacity) {
      const spaceAvailable = this.capacity - this.tokens;
      this.tokens += amount < spaceAvailable ? amount : spaceAvailable;
    }
  }

  /** Returns true if tokens are available. false if not */
  take(amount = 1): boolean {
    this.#refill();

    if (this.tokens >= amount) {
      this.tokens -= amount;
      return true;
    }

    return false;
  }

  /** Throws an error if tokens are not available. */
  tryTake(amount = 1): void {
    this.#refill();
    if (this.tokens >= amount) {
      this.tokens -= amount;
    } else throw new TokenBucketError("Tokens", "Not enough tokens available.");
  }

  /** Returns a promise which resolves once the amount of tokens can be taken. */
  async waitTake(amount = 1): Promise<void> {
    if (this.capacity < amount)
      throw new TokenBucketError("Capacity", "Cannot take more tokens than the capacity of this bucket.");

    await delayCondition(
      () => {
        this.#refill();
        return this.tokens >= amount;
      },
      {
        checkInterval: this.fillInterval,
      }
    );

    this.take(amount);
  }

  get remainingTokens(): number {
    this.#refill();
    return this.tokens;
  }

  #refill() {
    const now = Date.now();
    const amountToAdd = Math.floor((now - this.lastFilled) / this.fillInterval) * this.amountPerInterval;
    if (amountToAdd == 0) return;
    this.tokens = Math.min(this.capacity, this.tokens + amountToAdd);
    this.lastFilled = now;
  }
}
