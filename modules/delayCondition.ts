import type { AtLeastOne } from "../common/atLeastOne.ts";
import { sleep } from "./sleep.ts";

export type delayConditionFn = () => boolean | Promise<boolean>;

export async function delayCondition<T extends delayConditionFn>(
  condition: T,
  options: AtLeastOne<{
    /** Default: 500 ms */
    checkInterval: number;
    /** Max delay in ms */
    maxDelay?: number;
  }> = {
    checkInterval: 500,
  }
) {
  const failsafeTimestamp = options.maxDelay ? Date.now() + options.maxDelay : undefined;

  while (true) {
    if (failsafeTimestamp && Date.now() >= failsafeTimestamp) break;

    try {
      let res = condition();
      if (res instanceof Promise) res = await res;
      if (res == true) break;
      // deno-lint-ignore no-empty
    } catch {}

    await sleep(options.checkInterval!);
  }
  return;
}
